// Mega

#ifndef FILE_H
#define FILE_H

#include <string>

using std::string;

namespace file
{
	string Read(string filename);
}

#endif