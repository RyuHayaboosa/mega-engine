// Mega

#pragma once

#ifndef JSON_KEYWORDS_H
#define JSON_KEYWORDS_H

namespace json
{
	namespace keyword
	{
		const char *const Include = "Include";
		const char *const CameraArray = "cameras";
		const char *const PipelineStateArray = "pipelineStates";
		const char *const RenderableArray = "renderables";
	}
}

#endif /* JSON_KEYWORDS_H */